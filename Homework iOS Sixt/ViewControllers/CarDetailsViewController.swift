//
//  CarDetailsViewController.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/27/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import MapKit
import Kingfisher

class CarDetailsViewController: UIViewController {

    // MARK: - Public properties

    var car: Car?

    // MARK: - Outlets

    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var licensePlateView: UIView! {
        didSet {
            licensePlateView.layer.cornerRadius = 4
            licensePlateView.layer.borderColor = UIColor.black.cgColor
            licensePlateView.layer.borderWidth = 2
            licensePlateView.clipsToBounds = true
        }
    }
    @IBOutlet weak var licenseNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var modelNameLabel: UILabel!
    @IBOutlet weak var transmissionLabel: UILabel!
    @IBOutlet weak var fuelLabel: UILabel!
    @IBOutlet weak var tankFillLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!

    // MARK: - Lifecycle

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fillInfo()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mapView.removeAllAnnotations()
    }

    // MARK: - Private methods

    private func fillInfo() {
        guard let car = self.car else { return }
        licenseNumberLabel.text = car.licensePlate
        nameLabel.text = car.name
        modelNameLabel.text = car.modelName
        transmissionLabel.text = car.transmission.description
        fuelLabel.text = car.fuelType.description
        tankFillLabel.text = "\(car.fuelLevel * 100)%"

        setPointOnMap(for: car)

        guard let carImageUrl = URL(string: car.carImageUrl) else { return }
        carImageView.kf.setImage(with: carImageUrl)
    }

    private func setPointOnMap(for car: Car) {
        let annotation = MKPointAnnotation.init()
        let coordinate = CLLocationCoordinate2D(latitude: car.latitude, longitude: car.longitude)
        let title = car.modelName
        annotation.coordinate = coordinate
        annotation.title = title
        self.mapView.addAnnotation(annotation)

        let deltaCoordinates = 0.1
        let span = MKCoordinateSpan(latitudeDelta: deltaCoordinates, longitudeDelta: deltaCoordinates)
        let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
        mapView.setRegion(region, animated: false)
    }
}
