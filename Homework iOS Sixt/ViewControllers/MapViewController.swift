//
//  ViewController.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/23/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import MapKit

final class MapViewController: UIViewController {

    // MARK: - Constants

    private let downloadService = DownloadService()

    // MARK: - Outlets

    @IBOutlet weak var mapView: MKMapView!

    // MARK: - Lifecycle

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCarsArray()
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.mapView.removeAllAnnotations()
    }

    // MARK: - Private methods

    private func fetchCarsArray() {
        downloadService.downloadCarList{ [weak self] (cars) in
            self?.setupMapView(cars)
        }
    }

    private func setupMapView(_ carsArray: [Car]?) {
        setTitle(numberOfItems: carsArray?.count)
        guard let carsArray = carsArray, !carsArray.isEmpty else {
            mapView.removeAllAnnotations()
            return
        }
        let annotations: [MKPointAnnotation] = carsArray.compactMap { createAnnotation(car: $0) }
        mapView.showAnnotations(annotations, animated: true)
    }

    private func createAnnotation(car: Car) -> MKPointAnnotation {
        let annotation = MKPointAnnotation.init() 
        let coordinate = CLLocationCoordinate2D(latitude: car.latitude, longitude: car.longitude)
        let title = car.modelName
        annotation.coordinate = coordinate
        annotation.title = title
        return annotation
    }

    private func setTitle(numberOfItems: Int?) {
        guard let numberOfItems = numberOfItems else {
            self.navigationController?.navigationBar.topItem?.title = "No cars available now"
            return
        }
        self.navigationController?.navigationBar.topItem?.title = "Available \(numberOfItems) cars"
    }
}
