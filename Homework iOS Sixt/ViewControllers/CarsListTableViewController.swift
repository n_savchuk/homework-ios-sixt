//
//  CarsListTableViewController.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/27/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import UIKit

final class CarsListTableViewController: UITableViewController {

    // MARK: - Properties

    private var carsArray: [Car] = [] {
        didSet {
            self.navigationController?.navigationBar.topItem?.title = "Available \(carsArray.count) cars"
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Constants

    private let downloadService = DownloadService()
    private let rowHeight: CGFloat = 90
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        fetchCarsArray()
    }
    
    // MARK: - Private methods
    
    private func fetchCarsArray() {
        downloadService.downloadCarList{ [weak self] (cars) in
            guard let cars = cars else { return }
            self?.carsArray = cars
        }
    }
    
    private func registerCell() {
        tableView.register(CarDetailsTableViewCell.nib, forCellReuseIdentifier: CarDetailsTableViewCell.identifier)
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carsArray.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CarDetailsTableViewCell.identifier, for: indexPath) as? CarDetailsTableViewCell else {
            return UITableViewCell()
        }
        let car = carsArray[indexPath.item]
        cell.car = car

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let carDetailsViewController = UIStoryboard.viewController(from: .main, type: CarDetailsViewController.self) else { return }
        let car = carsArray[indexPath.item]
        carDetailsViewController.car = car

        self.navigationController?.pushViewController(carDetailsViewController, animated: true)
    }
}
