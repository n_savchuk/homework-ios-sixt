//
//  DownloadManager.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/27/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import Foundation

final class DownloadManager: NSObject, URLSessionDownloadDelegate {
    
    // MARK: - Private properties

    private lazy var session: URLSession = {
        let identifier = "backgroundSessionIdentifier"
        let config = URLSessionConfiguration.background(withIdentifier: identifier)
        config.urlCache = nil
        return URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
    }()
    
    private var completionHandler: ((Bool) -> Void)?
    
    // MARK: - Public methods

    func downloadFileWith(_ url: URL, completion:@escaping (Bool) -> Void) {
        self.completionHandler = completion
        let downloadTask = session.downloadTask(with: url)
        downloadTask.resume()
    }
    
    // MARK: - URLSessionDownloadDelegate

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let fileManager = FileManager.default
        guard let sourceURL = downloadTask.originalRequest?.url, let destinationURL = fileManager.getFileAt(path: sourceURL) else {
            return
        }
        
        try? fileManager.removeItem(at: destinationURL)
        do {
            try fileManager.copyItem(at: location, to: destinationURL)
            completionHandler?(true)
        } catch {
            completionHandler?(false)
        }
    }
}

