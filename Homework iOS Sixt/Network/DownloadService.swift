//
//  DownloadService.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/27/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import Foundation

final class DownloadService {
    
    // MARK: - FileEndpoint
    
    private enum FileEndpoint: String {
        case cars = "cars.json"
    }
    
    // MARK: - Public Constants

    private let downloadManager = DownloadManager()
    private let fileManager = FileManager.default
    
    // MARK: - Public Constants

    let globalUrlString = "http://www.codetalk.de/"
    
    // MARK: - Public Methods

    func downloadCarList(completion: @escaping ([Car]?) -> Void) {
        let urlString = globalUrlString + FileEndpoint.cars.rawValue
        
        guard let url = URL(string: urlString), let destinationURL = fileManager.getFileAt(path: url) else {
            return completion(nil)
        }
        
        do {
            let jsonData = try Data(contentsOf: destinationURL)
            let cars = try JSONDecoder().decode([Car].self, from: jsonData)
            completion(cars)
        } catch {
            downloadManager.downloadFileWith(url) { (completed) in
                guard completed else { return }
                DispatchQueue.main.async {
                    do {
                        let jsonData = try Data(contentsOf: destinationURL)
                        let cars = try JSONDecoder().decode([Car].self, from: jsonData)
                        completion(cars)
                    } catch {
                        completion(nil)
                    }
                }
            }
        }
    }
}
