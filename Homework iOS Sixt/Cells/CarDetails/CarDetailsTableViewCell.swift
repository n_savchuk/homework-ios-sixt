//
//  CarDetailsTableViewCell.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/27/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import UIKit
import Kingfisher

class CarDetailsTableViewCell: UITableViewCell {

    // MARK: - Outlets

    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carModelNameLabel: UILabel!
    @IBOutlet weak var fuelTypeLabel: UILabel!
    @IBOutlet weak var transmissionTypeLabel: UILabel!

    // MARK: - Public properties

    var car: Car? {
        didSet {
            guard let car = car else { return }
            setupUI(car: car)
        }
    }

    // MARK: - Private methods
    
    private func setupUI(car: Car) {
        carModelNameLabel.text = car.modelName
        fuelTypeLabel.text = car.fuelType.description
        transmissionTypeLabel.text = car.transmission.description
        
        guard let imageUrl = URL(string: car.carImageUrl) else { return }
        carImageView.kf.setImage(with: imageUrl)
    }
}
