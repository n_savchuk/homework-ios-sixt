//
//  MKMapView+Extension.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/27/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import MapKit

extension MKMapView {
    func removeAllAnnotations() {
        self.removeAnnotations(annotations)
    }
}
