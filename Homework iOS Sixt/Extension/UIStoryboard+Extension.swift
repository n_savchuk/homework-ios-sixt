//
//  UIStoryboard+Extension.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/27/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import UIKit

enum UIStoryboardNames : String {
    case main = "Main"
}

extension UIStoryboard {
    class func viewController<T>(from storyboard: UIStoryboardNames, type: T.Type) -> T? where T: UIViewController {
        return UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateViewController(withIdentifier: String(describing: type.self)) as? T
    }
}
