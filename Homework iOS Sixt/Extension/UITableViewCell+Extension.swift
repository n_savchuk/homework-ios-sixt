//
//  UITableViewCell+Extension.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/27/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
