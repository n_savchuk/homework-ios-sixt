//
//  FileManager+Extension.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/27/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import Foundation

extension FileManager {
    func getFileAt(path url: URL) -> URL? {
        let documentsPath = self.urls(for: .documentDirectory, in: .userDomainMask).first
        return documentsPath?.appendingPathComponent(url.lastPathComponent)
    }
}
