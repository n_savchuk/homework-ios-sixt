//
//  Car.swift
//  Homework iOS Sixt
//
//  Created by Nikita Savchuk on 1/23/19.
//  Copyright © 2019 savchuk.m. All rights reserved.
//

import UIKit

struct Car: Codable {
    
    typealias Identifier = String
    
    let id: Identifier
    let modelIdentifier: String
    let modelName: String
    let name: String
    let make: String
    let group: String
    let color: String
    let series: String
    let fuelType: FuelType
    let fuelLevel: Float
    let transmission: Transmission
    let licensePlate: String
    let latitude: Double
    let longitude: Double
    let innerCleanliness: Cleanliness
    let carImageUrl: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case modelIdentifier
        case modelName
        case name
        case make
        case group
        case color
        case series
        case fuelType
        case fuelLevel
        case transmission
        case licensePlate
        case latitude
        case longitude
        case innerCleanliness
        case carImageUrl
    }
}

extension Car {
    
    enum Transmission: String, Codable {
        case manual = "M"
        case automatic = "A"
        
        var description: String {
            switch self {
            case .automatic:
                return "Automatic"
            case .manual:
                return "Manual"
            }
        }
    }
    
    enum FuelType: String, Codable {
        case diesel = "D"
        case petrol = "P"
        case electic = "E"
        
        var description: String {
            switch self {
            case .diesel:
                return "Diesel"
            case .petrol:
                return "Petrol"
            case .electic:
                return "Electric"
            }
        }
    }
    
    enum Cleanliness: String, Codable {
        case regular = "REGULAR"
        case clean = "CLEAN"
        case veryClean = "VERY_CLEAN"
        
        var description: String {
            switch self {
            case .regular:
                return "Regular"
            case .clean:
                return "Clean"
            case .veryClean:
                return "Very clean"
            }
        }
    }
}

extension Car {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Identifier.self, forKey: .id)
        self.modelIdentifier = try container.decode(String.self, forKey: .modelIdentifier)
        self.modelName = try container.decode(String.self, forKey: .modelName)
        self.name = try container.decode(String.self, forKey: .name)
        self.make = try container.decode(String.self, forKey: .make)
        self.group = try container.decode(String.self, forKey: .group)
        self.color = try container.decode(String.self, forKey: .color)
        self.series = try container.decode(String.self, forKey: .series)
        self.fuelType = try container.decode(FuelType.self, forKey: .fuelType)
        self.fuelLevel = try container.decode(Float.self, forKey: .fuelLevel)
        self.transmission = try container.decode(Transmission.self, forKey: .transmission)
        self.licensePlate = try container.decode(String.self, forKey: .licensePlate)
        self.latitude = try container.decode(Double.self, forKey: .latitude)
        self.longitude = try container.decode(Double.self, forKey: .longitude)
        self.innerCleanliness = try container.decode(Cleanliness.self, forKey: .innerCleanliness)
        self.carImageUrl = try container.decode(String.self, forKey: .carImageUrl)
    }
}
